import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Names {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        boolean run = true;
        while(run == true){
        System.out.println("1: Add a name, 2: Display names 0: Quit");
        String input = sc.nextLine();
        if (input.equals("1")) {
            System.out.println("Enter a First and Last name: ");
            String name = sc.nextLine();

            try {
                FileWriter fileWriter = new FileWriter("filename.csv", true); //Set true for append mode
                PrintWriter printWriter = new PrintWriter(fileWriter);
                printWriter.println(name + ",");  //New line
                printWriter.close();
                System.out.println("Successfully wrote to the file.");
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
        if (input.equals("2")) {
            Scanner file = new Scanner(new File("filename.csv"));
            file.useDelimiter(",");   //sets the delimiter pattern
            System.out.println();
            while (file.hasNext()) //returns a boolean value
            {
                System.out.print(file.next());  //find and returns the next complete token from this scanner
            }
            file.close();  //closes the scanner
        }
        if(input.equals("0")){
            run = false;
        }
        }
    }
}
